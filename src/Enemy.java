import java.awt.*;
import java.awt.Component.*;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import javax.imageio.*;

public class Enemy {
	//Enemy class
	//Fields
	private String name;
	public boolean alive;
	private int direction;
	private int speed;
	private int posx;
	private int posy;
	private BufferedImage image;
	
	//Constructors
	public Enemy() {
		this.name = null;
		this.alive = true;
		this.direction = 0;
		this.speed = 1;
		this.posx = 0;
		this.posy=0;
		this.image = null;
	}
	public Enemy(String name, int direction, int speed, int posx, int posy) {
		this.name = name;
		this.alive = true;
		this.direction = direction;
		this.speed = speed;
		this.posx = posx;
		this.posy=posy;
	}
	
	//Accessors
	public int getDirection() {
		return this.direction;
	}
	public int getSpeed() {
		return this.speed;
	}

	public int getPosx() {
		return this.posx;
	}
	public int getPosy() {
		return this.posy;
	}
	
	//Mutators
    public void setSpeed(int newSpeed) {
	    this.speed = newSpeed;
    }

	public void setPosx(int newPos) {
		this.posx = newPos;
	}
	public void setPosy(int newPos) {
		this.posy = newPos;
	}

	public void setDirection(int direction) {
	    this.direction = direction;
    }
	
	
	
	//Behaviours
	public void move() {
			if (direction == 0) {
				this.setPosy(this.getPosy()+this.getSpeed());
			}
			else if (direction == 1) {
				this.setPosx(this.getPosx()+this.getSpeed());
				this.setPosy(this.getPosy()+this.getSpeed());
			}
			else if (direction == 2) {
				this.setPosx(this.getPosx()+this.getSpeed());
			}
			else if (direction == 3) {
				this.setPosx(this.getPosx()+this.getSpeed());
				this.setPosy(this.getPosy()-this.getSpeed());
			}
			else if (direction == 4) {
				this.setPosy(this.getPosy()-this.getSpeed());
			}
			else if (direction == 5) {
				this.setPosy(this.getPosy()-this.getSpeed());
				this.setPosx(this.getPosx()-this.getSpeed());
			}
			else if (direction == 6) {
				this.setPosx(this.getPosx()-this.getSpeed());
			}
			else if (direction == 7) {
				this.setPosx(this.getPosx()-this.getSpeed());
				this.setPosy(this.getPosy()+this.getSpeed());
			}
	}
	public void speedUp() {
		this.setSpeed(this.getSpeed() + 1);
	}
	public void kill() {
		//this = null;
		System.gc();
	}
	
}

/*
 * Direction key
 * 
 * 0 ^
 * 1 ^>
 * 2 >
 * 3 v>
 * 4 v
 * 5 v<
 * 6 <
 * 7 ^<
 * 
 */

