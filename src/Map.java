
import java.awt.*;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class Map extends JPanel {
    private Image map;

    private Image mapImage;

    private Image phoneImage;

    private Image skateboardImage;

    private Timer boardTimer;

    private int phoneX;
    private int phoneY;

    //1000 x 650

    private Player player; //96 x 142
    public Image backupPlayerImage;
    public Image playerImage;
    public Image ouchImage;
    public Image dudeWinImage;

    public Enemy board;

    private Random random = new Random();
    private int randomInt;

    public Map(Player player) {
        init();
        this.player = player;
    }

    public void init() {
        try {
        	this.phoneX = (int)(Math.random()*700+50);
            this.phoneY = (int)(Math.random()*500+50);

            this.playerImage = getToolkit().getImage(getClass().getResource("resource/cole.png"));
            this.backupPlayerImage = getToolkit().getImage(getClass().getResource("resource/cole.png"));
            this.ouchImage = getToolkit().getImage(getClass().getResource("resource/ouch.png"));
            this.dudeWinImage = getToolkit().getImage(getClass().getResource("resource/dudewin.png"));
            this.backupPlayerImage= getToolkit().getImage(getClass().getResource("resource/cole.png"));

            this.phoneImage = getToolkit().getImage(getClass().getResource("resource/phone.png"));
            this.mapImage = getToolkit().getImage(getClass().getResource("resource/kingston.png"));

            this.skateboardImage = getToolkit().getImage(getClass().getResource("resource/skateboard.png"));

            this.map = mapImage;

			Sound soundtrack = new Sound("resources/soundtrack.mp3");
			soundtrack.start();
        } catch (Exception e) {
            System.out.println("You messed up");
        }

        this.boardTimer = new Timer();
        

        spawnBoard();
    }

    public void checkCollision() {
        //System.out.println("Phone: X:" + this.phoneX + "Y:" + this.phoneY);
        //System.out.println("Player: X:" + this.player.getX());
        if ((this.player.getX() <= this.phoneX) && (this.player.getX()+96 >= this.phoneX +20) && (this.player.getY() <= this.phoneY) && (this.player.getY()+142 >= this.phoneY + 63)) {
            changePhone();
        }
        
        if ((this.board.getPosx()>this.player.getX() && this.board.getPosx()<this.player.getX()+96&&this.board.getPosy()>this.player.getY()&&this.board.getPosy()<this.player.getY()+142) || (this.board.getPosx()+47>this.player.getX() && this.board.getPosx()+47<this.player.getX()+96&&this.board.getPosy()+200>this.player.getY()&&this.board.getPosy()+200<this.player.getY()+142) || (this.board.getPosx()>this.player.getX() && this.board.getPosx()<this.player.getX()+96&&this.board.getPosy()+200>this.player.getY()&&this.board.getPosy()+200<this.player.getY()+142) || (this.board.getPosx()+47>this.player.getX() && this.board.getPosx()+47<this.player.getX()+96&&this.board.getPosy()+200>this.player.getY()&&this.board.getPosy()+200<this.player.getY()+142)) {
        	this.player.startOuchTimer();
        }
        
        if (this.player.getX() <= 0) {
            this.player.setX(this.player.getX() + 5);
        }

        if (this.player.getX() + 96 >= 1000) {
            this.player.setX(this.player.getX() - 5);
        }

        if (this.player.getY() >= 650) {
            this.player.setY(this.player.getY() - 5);
        }

        if (this.player.getY() <= 0) {
            this.player.setY(this.player.getY() + 5);
        }
    }

    public void changePhone() {
        this.player.setPhones(player.getPhones() + 1);

        Game.setLabel(Game.phones, Integer.toString(player.getPhones()));

        this.phoneX = (int)(Math.random()*700+50);
        this.phoneY = (int)(Math.random()*500+50);

        System.out.println("New Phone: X:" + this.phoneX + " Y:" + this.phoneY);
        this.player.startDudeWinTimer();

        Game.map.repaint();
    }

    //
    public void spawnBoard() {
        //this.boardTimer = new Timer();
    	this.board = new Enemy("board", 3, 4, 0, 0);
        this.boardTimer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                board.move();
                
                //Check if out of bounds
                if (Game.map.board.getPosx() < 0) {
                    if (Game.map.board.getDirection() == 5)
                        Map.this.board.setDirection(3);
                    if (Map.this.board.getDirection() == 7)
                        Map.this.board.setDirection(1);
                }
                if (Map.this.board.getPosx() > 800) {
                    if (Map.this.board.getDirection() == 3)
                        Map.this.board.setDirection(5);
                    if (board.getDirection() == 1)
                        board.setDirection(7);
                }
                if (board.getPosy() > 450) {
                    if (board.getDirection() == 1)
                        board.setDirection(3);
                    if (board.getDirection() == 7)
                        board.setDirection(5);
                }
                if (board.getPosy() < 0) {
                    if (board.getDirection() == 3)
                        board.setDirection(1);
                    if (board.getDirection() == 5)
                        board.setDirection(7);
                }

                //System.out.println("X:" + board.getPosx() + " Y:" + board.getPosy() + " Direction:" + board.getDirection());
                Game.map.repaint();
            }
        }, 0, 80 - Game.player.getPhones());
        //this.boardTimer.start();

}

    public void draw(Graphics g) {
        try {
            Graphics2D g2D;
            g2D = (Graphics2D) g;

            g2D.drawImage(this.map, 0, 0, this);
            g2D.drawImage(this.phoneImage, this.phoneX, this.phoneY, this);
            g2D.drawImage(this.playerImage, this.player.getX(), this.player.getY(), this);
            g2D.drawImage(this.skateboardImage, this.board.getPosx(), this.board.getPosy(), this);

            checkCollision();
        } catch(Exception e) {
            System.out.println(e);
        }
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        this.draw(g);
    }
}
