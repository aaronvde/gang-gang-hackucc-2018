import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.*;

public class Game {
    public static JFrame gameWindow =  new JFrame("Crazy Cole Murray's Skateboard Adventure");

    public static JPanel toolbar = new JPanel();

    public static InputMap inputMap = toolbar.getInputMap(JPanel.WHEN_IN_FOCUSED_WINDOW);

    public static ActionMap actionMap = toolbar.getActionMap();

    public static Player player = new Player();

    public static Map map = new Map(player);

    public static JLabel livesLabel = new JLabel("Lives:");
    public static JLabel lives = new JLabel("3");

    public static JLabel phonesLabel = new JLabel("Phones:");
    public static JLabel phones = new JLabel(Integer.toString(player.getPhones()));

    public void init() {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                guiApp();
            }
        });
    }

    public static void guiApp() {
        gameWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    //When window is closed, close all running threads

        gameWindow.getContentPane().add(map, BorderLayout.CENTER);    //Adds new canvas in the center of the gamewindow.
        gameWindow.getContentPane().add(toolbar, BorderLayout.SOUTH);

        toolbar.add(livesLabel);
        toolbar.add(lives);
        toolbar.add(phonesLabel);
        toolbar.add(phones);

        inputMap.put(KeyStroke.getKeyStroke("pressed W"), "W");
        inputMap.put(KeyStroke.getKeyStroke("pressed A"), "A");
        inputMap.put(KeyStroke.getKeyStroke("pressed S"), "S");
        inputMap.put(KeyStroke.getKeyStroke("pressed D"), "D");

        inputMap.put(KeyStroke.getKeyStroke("released W"), "w");

        actionMap.put("W", new KeyboardAction("W"));
        actionMap.put("A", new KeyboardAction("A"));
        actionMap.put("S", new KeyboardAction("S"));
        actionMap.put("D", new KeyboardAction("D"));

        gameWindow.pack();
        gameWindow.setSize(840, 700);
        gameWindow.setResizable(false);
        gameWindow.setVisible(true);
        
        /*gameWindow.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
                System.out.println("Key pressed code=" + e.getKeyCode() + ", char=" + e.getKeyChar());
				int id = e.getID();
				if (id == KeyEvent.VK_W) {
					Game.player.setCurrentlyMoving(true);
		            Game.player.moveUp();
				}
				if (id == KeyEvent.VK_A) {
					Game.player.setCurrentlyMoving(true);
		            Game.player.moveUp();
				}
				if (id == KeyEvent.VK_S) {
					Game.player.setCurrentlyMoving(true);
		            Game.player.moveUp();
				}
				if (id == KeyEvent.VK_D) {
					Game.player.setCurrentlyMoving(true);
		            Game.player.moveUp();
				}
				if ((id != KeyEvent.VK_W)||(id != KeyEvent.VK_A)||(id != KeyEvent.VK_S)||(id != KeyEvent.VK_D)) {
					Game.player.setCurrentlyMoving(false);
				}
			}
        	@Override
            public void keyPressed(KeyEvent e) {
                System.out.println("Key pressed code=" + e.getKeyCode() + ", char=" + e.getKeyChar());
                int id = e.getID();
				if (id == KeyEvent.VK_W) {
					Game.player.setCurrentlyMoving(true);
		            Game.player.moveUp();
				}
				if (id == KeyEvent.VK_A) {
					Game.player.setCurrentlyMoving(true);
		            Game.player.moveUp();
				}
				if (id == KeyEvent.VK_S) {
					Game.player.setCurrentlyMoving(true);
		            Game.player.moveUp();
				}
				if (id == KeyEvent.VK_D) {
					Game.player.setCurrentlyMoving(true);
		            Game.player.moveUp();
				}
				if ((id != KeyEvent.VK_W)||(id != KeyEvent.VK_A)||(id != KeyEvent.VK_S)||(id != KeyEvent.VK_D)) {
					Game.player.setCurrentlyMoving(false);
				}
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }

        });*/
    }

    public static void setLabel(JLabel label, String string) {
        label.setText(string);
    }

}