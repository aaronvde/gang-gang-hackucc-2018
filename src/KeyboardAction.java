import javax.swing.*;
import java.awt.event.ActionEvent;

public class KeyboardAction extends AbstractAction {

    private String command;

    public KeyboardAction(String command) {
        this.command = command;
    }

    public void actionPerformed(ActionEvent event) {
        if (command.equals("W")) {
            Game.player.setCurrentlyMoving(true);
            Game.player.moveUp();
        } else if (command.equals("A")) {
            Game.player.setCurrentlyMoving(true);
            Game.player.moveLeft();
        } else if (command.equals("S")) {
            Game.player.setCurrentlyMoving(true);
            Game.player.moveDown();
        } else if (command.equals("D")) {
            Game.player.setCurrentlyMoving(true);
            Game.player.moveRight();
        } else if (command.equals("w")) {
            Game.player.setCurrentlyMoving(false);
        }
    }
}
