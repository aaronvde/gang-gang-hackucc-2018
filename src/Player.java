import java.util.Timer;
import java.util.TimerTask;

public class Player {
    private int lives = 3;
    private int phones = 0;
    public int speed = 5;

    private int x = 250;
    private int y = 300;

    private boolean currentlyMoving = false;

    private Timer ouchTimer;
    private Timer dudeWinTimer;
//
    public Player() {
		Sound soundtrack = new Sound("resources/soundtrack.mp3");
		soundtrack.start();
        Timer ouchTimer = new Timer();
        Timer dudeWinTimer = new Timer();
    }

    public int getPhones() {
        return this.phones;
    }

    public void setPhones(int phones) {
        this.phones = phones;
    }

    public int getPhone() {
        return this.phones;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getX() {
        return this.x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getY() {
        return this.y;
    }

    public boolean isCurrentlyMoving() {
        if (this.currentlyMoving) {
            currentlyMoving = false;
            return currentlyMoving;
        } else {
            currentlyMoving = true;
            return currentlyMoving;
        }
    }

    public void setCurrentlyMoving(boolean currentlyMoving) {
        this.currentlyMoving = currentlyMoving;
    }

    public void moveUp() {
        if (this.currentlyMoving) {
            this.y = this.y - speed;
            Game.map.repaint();
        }
    }

    public void moveDown() {
        if (this.currentlyMoving) {
            this.y = this.y + speed;
            Game.map.repaint();
        }
    }

    public void moveLeft() {
        if (this.currentlyMoving) {
            this.x = this.x - speed;
            Game.map.repaint();
        }
    }

    public void moveRight() {
        if (this.currentlyMoving) {
            this.x = this.x + speed;
            Game.map.repaint();
        }
    }

    public void death() {
        System.out.println("Cole is dead, dude fail.");
        this.speed = 5;

        if (this.lives > 1) {
            lives--;
            Game.setLabel(Game.lives, Integer.toString(lives));

            this.x = 450;
            this.y = 450;
        } else {
            System.exit(0);
        }
    }

    public void startOuchTimer() {
    	death();
    	Game.map.spawnBoard();
        Game.map.playerImage = Game.map.ouchImage;

        (new Thread() {
        	public void run() {
        		// do stuff
        		try {
        			Thread.sleep(1000);
        		} catch (Exception e) {System.out.println(e);}
        		Game.map.playerImage = Game.map.backupPlayerImage;
        	}
        }).start();
    }

    public void startDudeWinTimer() {
    	Game.map.board.setSpeed(Game.map.board.getSpeed()+1);
    	this.speed++;
        Game.map.playerImage = Game.map.dudeWinImage;
        Game.map.repaint();
        //Delay here
        (new Thread() {
        	public void run() {
        		// do stuff
        		try {
        			Thread.sleep(1000);
        		} catch (Exception e) {System.out.println(e);}
        		Game.map.playerImage = Game.map.backupPlayerImage;
        	}
        }).start();

        Game.map.repaint();
    }

}