import javax.swing.*;
import javax.swing.GroupLayout.Alignment;

import java.awt.*;
import java.awt.event.*;
import javax.swing.JLabel;

public class NewMenu
{
    static JFrame titleFrame = new JFrame ("Platformer");
    static JFrame instructionFrame = new JFrame ("Platformer");
    static JFrame creditsFrame = new JFrame ("Platformer");

    static JLabel titleLabel = new JLabel ("Crazy Cole Murray's Great Escape"); //title screen
    static JLabel instructionLabel = new JLabel ("Collect all the cellphone%nnumbers so Cole can have a date!%nDon't let the skateboard%nbreak your arm!%nUse WASD to move");
    static JLabel creditsLabel = new JLabel ("Player Programming: Daniel Bala\nEnemy Programming: Aaron vandenEndennmGUI: Chris Rupert\nStarring: Cole Murray");


    static JButton play = new JButton ("Play");
    static JButton instructions = new JButton ("Instructions");
    static JButton credits = new JButton ("Credits");

    static JButton back[] = new JButton [2];



    static JPanel titlePanel = new JPanel ();
    static JPanel instructionPanel = new JPanel ();
    static JPanel creditsPanel = new JPanel ();

    static ButtonHandler onClick = new ButtonHandler ();

    static Font newFont = new Font("Comic Sans MS", Font.BOLD, 36);
    static Font myFont = new Font ("Forte", Font.BOLD, 24);

    private static void guiApp ()
    {
	titlePanel.removeAll (); //remove all makes it so that the panel displays each time you go back and forth
	instructionPanel.removeAll ();
	creditsPanel.removeAll ();

	starter (); //main menu options
	instruction ();
	credit ();
    }


    public static class ButtonHandler implements ActionListener //checking for buttons clicked
    { //class start

	public void actionPerformed (ActionEvent e)
	{
	    Object source = e.getSource (); //object is whatever button is clicked

		if (e.getSource() == play) {
			Sound soundtrack = new Sound("sresources/soundtrack.mp3");
			soundtrack.start();
			Game game = new Game();
			game.init();
		} else if (e.getSource () == instructions) //game instructions
	    {
		titleFrame.setVisible (false);
		instructionFrame.setVisible (true);
	    }

	    else if (e.getSource () == credits) //game credits
	    {
		titleFrame.setVisible (false);
		creditsFrame.setVisible (true);
	    }

	    else if (e.getSource () == back [0] || e.getSource () == back [1]) //two backs, one for instructions one for credits
	    {
		creditsFrame.setVisible (false);
		instructionFrame.setVisible (false);
		titleFrame.setVisible (true);

	    }


	}
    }


    public static void starter ()
    {
	titlePanel.setLayout (new BoxLayout (titlePanel, BoxLayout.PAGE_AXIS));

	titleFrame.setBackground (Color.black);
	titlePanel.setBackground (Color.black);
	play.setBackground (Color.black);
	play.setForeground (Color.red);
	instructions.setBackground (Color.black);
	instructions.setForeground (Color.red);
	credits.setBackground (Color.black);
	credits.setForeground (Color.red);

	for (int x = 0 ; x < 2 ; x++) //displaying 2 back buttons because 2 are used
	{
	    back [x] = new JButton ("Back");
	    back [x].setFont (myFont);
	    back [x].setForeground (Color.blue);
	    back [x].setBackground (Color.white);
	    back [x].addActionListener (onClick);
	}

	titlePanel.setPreferredSize (new Dimension (500, 1000));
	titleFrame.setBounds (700, 100, 635, 635);

	play.setFont (myFont);
	instructions.setFont (myFont);
	credits.setFont (myFont);

	play.addActionListener (onClick); //action listeners for each button
	instructions.addActionListener (onClick);
	credits.addActionListener (onClick);
	
	titlePanel.setAlignmentX(titlePanel.CENTER_ALIGNMENT);
	play.setHorizontalAlignment(play.CENTER);
	instructions.setHorizontalAlignment(instructions.CENTER);
	credits.setHorizontalAlignment(instructions.CENTER);
	
	titlePanel.add (titleLabel, BorderLayout.NORTH);
	titlePanel.add (play, BorderLayout.NORTH);
	titlePanel.add (instructions, BorderLayout.NORTH);
	titlePanel.add (credits, BorderLayout.NORTH);

	titleFrame.getContentPane ().add (titlePanel, BorderLayout.NORTH);
	titleFrame.setVisible (true);
	titleLabel.setFont(newFont);
	titleLabel.setForeground(Color.WHITE);
    }


    public static void instruction ()
    {
	instructionPanel.setLayout (new BoxLayout (instructionPanel, BoxLayout.PAGE_AXIS));
	instructionPanel.setPreferredSize (new Dimension (635, 1000));
	instructionFrame.setBounds (700, 100, 635, 635);
	instructionPanel.add (instructionLabel, BorderLayout.NORTH);
	instructionPanel.add (back [0], BorderLayout.NORTH);
	instructionPanel.setBackground (Color.black);

	instructionLabel.setFont (myFont);
	instructionLabel.setForeground (Color.red);

	instructionFrame.getContentPane ().add (instructionPanel, BorderLayout.NORTH);

    }


    public static void credit ()
    {
	creditsPanel.setLayout (new BoxLayout (creditsPanel, BoxLayout.PAGE_AXIS));
	creditsPanel.setPreferredSize (new Dimension (635, 1000));
	creditsFrame.setBounds (700, 100, 550, 635);
	creditsPanel.add (creditsLabel, BorderLayout.NORTH);
	creditsPanel.add (back [1], BorderLayout.SOUTH);
	creditsPanel.setBackground (Color.black);

	creditsPanel.setAlignmentX (JPanel.CENTER_ALIGNMENT);

	creditsLabel.setFont (myFont);
	creditsLabel.setForeground (Color.red);

	creditsFrame.getContentPane ().add (creditsPanel, BorderLayout.NORTH);
    }


    public static void main (String[] args)
    {
	//Schedule a job for the event-dispatching thread:
	//creating and showing this application's GUI.
	javax.swing.SwingUtilities.invokeLater (new Runnable ()
	{
	    public void run ()
	    {
		guiApp ();
	    }
	}
	);
    }
}

